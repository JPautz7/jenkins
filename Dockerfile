FROM jenkins/jenkins:lts
USER root

RUN curl https://releases.hashicorp.com/terraform/0.14.5/terraform_0.14.5_linux_amd64.zip --output /tmp/terraform.zip \
  && unzip /tmp/terraform.zip -d /usr/bin \
  && rm /tmp/terraform.zip

RUN curl https://releases.hashicorp.com/packer/1.6.6/packer_1.6.6_linux_amd64.zip --output /tmp/packer.zip \
  && unzip /tmp/packer.zip -d /usr/bin \
  && rm /tmp/packer.zip

USER jenkins