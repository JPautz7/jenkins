### Jenkins
Note: The local mount is at `/var/jenkins_home` the `jenkins` user `1000:1000` will need permissions
`sudo chown -R 1000:1000 /var/jenkins_home`

You will first have to run
`docker build -t jenkins-terraform-packer .` before you can deploy `kubectl apply -f .`

Typically these dependencies would not be baked into the image but rather would be called during their own containerized step using the kubernates plugin
